/*
 * Takes an array from the user and uses linear search to find
 * a specified element
 */
#include <stdio.h>
#include <conio.h>

#define SIZE 5

void main() {
    int i, element, index = -1;
    int ar[SIZE];

    clrscr();

    /* Input start */

    for (i = 0; i < SIZE; i++) {
        printf("Enter the value of element %d: ", i);
        scanf("%d", &ar[i]);
    }

    printf("Enter the value to search for: ");
    scanf("%d", &element);

    /* Input end */

    /* Linear search start */

    for (i = 0; i < SIZE; i++) {
        if (ar[i] == element) {
            index = i;
        }
    }

    if(index > 0) {
        printf("The element %d was found at index %d", element, index);
    } else {
        printf("The element %d was not found in the array", element);
    }

    getch();
}