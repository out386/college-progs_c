/*
 * Takes a number from the user and finds the sum of the digits
 */
#include <stdio.h>
#include <conio.h>

void main() {
    int num, sum = 0;
    clrscr();
    printf("Enter a number: ");
    scanf("%d", &num);

    while (num > 0) {
        sum = sum + num % 10;
        num = num / 10;
    }

    printf("The sum of the digits of the number is %d ", sum);
    getch();
}