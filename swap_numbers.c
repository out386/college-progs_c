/*
 * Takes two numbers from the user and swaps them
 * without using a third variable
 */
#include <stdio.h>
#include <conio.h>

void main() {
    int num1, num2;
    clrscr();
    printf("Enter two numbers: ");
    scanf("%d %d", &num1, &num2);

    num1 = num1 + num2;
    num2 = num1 - num2;
    num1 = num1 - num2;

    printf("The swapped numbers are %d and %d ", num1, num2);
    getch();
}