/*
 * Takes an array from the user and uses macros to find
 * the smallest and largest element
 */

#include <stdio.h>
#include <conio.h>

#define SIZE 5
#define MIN(x,y) (x) < (y) ? (x) : (y);
#define MAX(x,y) (x) > (y) ? (x) : (y);

void main() {
    int i, min, max;
    int ar[SIZE];

    clrscr();

    for (i = 0; i < SIZE; i++) {
        printf("Enter the value of element %d: ", i);
        scanf("%d", &ar[i]);
    }

    min = max = ar[0];
    
    for (i = 0; i < SIZE; i++) {
        min = MIN(min,ar[i]);
        max = MAX(max,ar[i]);
    }

    printf("The smallest element is %d\n", min);
    printf("The largest element is %d\n", max);


    getch();
}