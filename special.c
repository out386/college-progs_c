/*
 * Takes a number from the user and checks if it is aspecial number
 */
#include <stdio.h>
#include <conio.h>

void main() {
    int n, nc, r, sum = 0;
    
    int factorial(int);

    clrscr();
    printf("Enter a number: ");
    scanf("%d", &n);
    nc = n;
    
    while(n > 0) {
        r = n % 10;
        sum = sum + factorial(r);
        n = n / 10;
    }

    if (sum == nc) 
        printf("%d is a special number", nc);
    else
        printf("%d is not a special number", nc);
    getch();
}

int factorial(int n) {
    int fact = 1;
    int i;
    if (n == 0)
        return 0;
    
    for (i = 2; i <=n; i++) {
        fact = fact * i;
    }
    return fact;
}