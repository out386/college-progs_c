/*
 * Takes a number from the user and checks if it is an Armstrong number
 */
#include <stdio.h>
#include <conio.h>
#include <math.h>

void main() {
    int n, nc, nd = 0, sum = 0, r;

    clrscr();
    printf("Enter a number: ");
    scanf("%d", &n);
    nc = n;
    
    while(n > 0) {
        n = n / 10;
        nd++;
    }
    n = nc;
    while (n > 0) {
        r = n % 10;
        sum = sum + pow(r, nd);
        n = n / 10;
    }

    if (sum == nc) 
        printf("%d is an Armstrong number", nc);
    else
        printf("%d is not an Armstrong number", nc);
    getch();
}
