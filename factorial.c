/*
 * Takes a number from the user and finds its factorial
 */
#include <stdio.h>
#include <conio.h>

void main() {
    int n;
    long fact;
    long factorial(int);

    clrscr();
    printf("Enter a number: ");
    scanf("%d", &n);

    if (n < 0)
        printf("Wrong input");
    else {
        fact = factorial(n);
        printf("The factorial of %d is %ld", n, fact);
    }
    getch();
}

long factorial(int n) {
    int i;
    long fact = 1;
    for (i = 2; i <= n; i++)
        fact = fact * i;
    return fact;
}
