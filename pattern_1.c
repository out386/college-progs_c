/*
 * Prints the following pattern:
 * * * * * *
 *  * * * *
 *   * * *
 *    * *
 *     *
 */
#include <stdio.h>
#include <conio.h>

void main() {
    int n, i, j, k;
    clrscr();
    printf("Enter the number of lines: ");
    scanf("%d", &n);

    for (i = 0; i < n; i++) {
        for (j = 0; j < i; j++)
            printf(" ");
        for (k = n; k > i; k--)
            printf("* ");
        printf("\n");
    }

    getch();
}