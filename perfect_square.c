/*
 * Takes a number from the user and checks if it is a perfect square
 */
#include <stdio.h>
#include <conio.h>
#include <math.h>

void main() {
    int num, root;
    clrscr();
    printf("Enter a number: ");
    scanf("%d", &num);

    root = sqrt(num);
    if (root * root == num) {
        printf("%d is a perfect square", num);
    } else {
        printf("%d is not a perfect square", num);
    }

    getch();
}