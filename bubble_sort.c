/*
 * Takes an array from the user and sorts it with bubble sort
 */
#include <stdio.h>
#include <conio.h>

#define SIZE 5

void main() {
    int i, j, temp;
    int ar[SIZE];

    clrscr();

    for (i = 0; i < SIZE; i++) {
        printf("Enter the value of element %d: ", i);
        scanf("%d", &ar[i]);
    }


    for (i = 0; i < SIZE; i++) {
        for (j = 0; j < SIZE - i - 1; j++) {
            if (ar[j] > ar[j +1 ]) {
                temp = ar[j];
                ar[j] = ar[j + 1];
                ar[j + 1] = temp;
            }
        }
    }


    printf("The sorted array is:\n");
    for (i = 0; i < SIZE; i++)
        printf("%d ", ar[i]);
    getch();
}
