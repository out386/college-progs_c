/*
 * Takes a number from the user and reverses the order of the digits
 */
#include <stdio.h>
#include <conio.h>

void main() {
    int num, rev = 0;
    clrscr();
    printf("Enter the number to reverse: ");
    scanf("%d", &num);

    while (num > 0) {
        rev = rev * 10 + num % 10;
        num = num / 10;
    }

    printf("The reversed number is: %d ", rev);
    getch();
}